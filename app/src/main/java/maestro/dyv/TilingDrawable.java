package maestro.dyv;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;

public class TilingDrawable extends android.support.v7.graphics.drawable.DrawableWrapper {

    public final String TAG = getClass().getSimpleName();

    private ColorFilter mColorFilter;

    private int mAlpha = 255;
    private int mFillColor = Color.TRANSPARENT;

    private boolean callbackEnabled = true;

    private int offsetTop = 0;

    public TilingDrawable(Context context, int drawable) {
        super(context.getResources().getDrawable(drawable));
    }

    public TilingDrawable(Drawable drawable) {
        super(drawable);
    }

    @Override
    public void draw(Canvas canvas) {
        callbackEnabled = false;
        Rect bounds = getBounds();
        Drawable wrappedDrawable = getWrappedDrawable();

        if (mFillColor != Color.TRANSPARENT) {
            canvas.drawColor(mFillColor);
        }

        int width = wrappedDrawable.getIntrinsicWidth();
        int height = wrappedDrawable.getIntrinsicHeight();

        int yStart = bounds.top - offsetTop;
        if (yStart > 0) {
            yStart = yStart - height;
        }

        for (int y = yStart; y < bounds.bottom + height - 1; y += height) {
            for (int x = bounds.left; x < bounds.right + width - 1; x += width) {
                if (mColorFilter != null) {
                    wrappedDrawable.setColorFilter(mColorFilter);
                }
                wrappedDrawable.setAlpha(mAlpha);
                wrappedDrawable.setBounds(x, y, x + width, y + height);
                wrappedDrawable.draw(canvas);
            }
        }
        callbackEnabled = true;
    }

    public void setOffsetTop(int offsetTop) {
        this.offsetTop = offsetTop;
        invalidateSelf();
    }

    @Override
    public void setAlpha(int alpha) {
        super.setAlpha(alpha);
        mAlpha = alpha;
    }

    @Override
    public void setColorFilter(ColorFilter cf) {
        super.setColorFilter(cf);
        mColorFilter = cf;
    }

    public void setFillColor(int color) {
        mFillColor = color;
    }

    public void invalidateDrawable(Drawable who) {
        if (callbackEnabled) {
            super.invalidateDrawable(who);
        }
    }

    public void scheduleDrawable(Drawable who, Runnable what, long when) {
        if (callbackEnabled) {
            super.scheduleDrawable(who, what, when);
        }
    }

    public void unscheduleDrawable(Drawable who, Runnable what) {
        if (callbackEnabled) {
            super.unscheduleDrawable(who, what);
        }
    }

}
