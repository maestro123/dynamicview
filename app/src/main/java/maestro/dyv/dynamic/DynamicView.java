package maestro.dyv.dynamic;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.animation.Interpolator;
import android.view.animation.OvershootInterpolator;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import maestro.dyv.R;

/**
 * Created by maestro123 on 22.12.2016.
 */

public class DynamicView extends View {

    public static final String TAG = DynamicView.class.getSimpleName();

    private final RectF mOriginRect = new RectF();

    private final LinkedHashMap<Integer, DynamicObject> mObjects = new LinkedHashMap<>();
    private final LinkedHashMap<Integer, DynamicTransition> mTransitions = new LinkedHashMap<>();

    private DynamicTransition mSingleTransition;

    private OnTransitionUpdateListener mTransitionUpdateListener;

    private int mRectWidth = 0;
    private int mRectHeight = 0;

    private int mRequestedBottomPadding = 0;

    private float mDensity = Integer.MIN_VALUE;
    private float mTransitionPercent = 0f;

    private boolean DEBUG = false;

    public DynamicView(Context context) {
        this(context, null);
    }

    public DynamicView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public DynamicView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        if (attrs != null) {
            final TypedArray array = context.obtainStyledAttributes(attrs, R.styleable.DynamicView);

            final String[] rect = array.getString(R.styleable.DynamicView_rect).split("x");
            mRectWidth = Integer.valueOf(rect[0]);
            mRectHeight = Integer.valueOf(rect[1]);

            array.recycle();
        }

        setDestiny(getResources().getDisplayMetrics().density);

//        setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                jump(!isVisible, null, true);
//            }
//        });

    }

    public void setOnTransitionUpdateListener(OnTransitionUpdateListener listener) {
        mTransitionUpdateListener = listener;
    }

    private void setDestiny(float destiny) {
        if (mDensity != destiny) {
            mDensity = destiny;
            mOriginRect.set(0, 0, mRectWidth * mDensity, mRectHeight * mDensity);
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int width = MeasureSpec.getSize(widthMeasureSpec);
        int height = MeasureSpec.getSize(heightMeasureSpec);
        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);

        int w = width;
        if (widthMode != MeasureSpec.EXACTLY) {
            w = (int) mOriginRect.width();
        }
        int h = height;
        if (heightMode != MeasureSpec.EXACTLY) {
            h = (int) mOriginRect.height();
        }

        setMeasuredDimension(w, h);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        final int width = getWidth();
        final int height = getHeight() - getPaddingTop() - getPaddingBottom();

        if (DEBUG) {
            Paint paint = new Paint();
            paint.setColor(Color.YELLOW);

            canvas.drawRect(new RectF(0, getPaddingTop(), width, height + getPaddingTop()), paint);
        }

        final float originWidth = mOriginRect.width();
        final float originHeight = mOriginRect.height();

        float fitScale = 1f;
        if (mOriginRect.width() > width || mOriginRect.height() > height) {
            if (width < height) {
                fitScale = width / originWidth;
            } else {
                fitScale = height / originHeight;
            }
            canvas.scale(fitScale, fitScale);
            fitScale = 1f / fitScale;
        }

        float topPadding = getPaddingTop() * fitScale;
        float requestedBottomPadding = mRequestedBottomPadding * fitScale;

        final float leftShift = (width * fitScale - originWidth) / 2;
        float topShift = (height * fitScale - originHeight) / 2 + topPadding;

        if (topShift > 0 && requestedBottomPadding + topPadding > topShift) {
            topShift = Math.max(topPadding, topShift - (requestedBottomPadding + topPadding - topShift));
        }

        canvas.save();
        canvas.translate(leftShift, topShift);

        if (DEBUG) {
            Paint paint = new Paint();
            paint.setColor(Color.RED);
            canvas.drawRect(mOriginRect, paint);

            paint.setColor(Color.BLUE);
            canvas.drawRect(new RectF(mOriginRect.left - mOriginRect.width(), mOriginRect.top, mOriginRect.left, mOriginRect.bottom), paint);
        }

        for (DynamicObject dob : mObjects.values()) {

            DynamicTransition tr = mTransitions.get(dob.id);

            int left = (int) (dob.x * mDensity);
            int top = (int) (dob.y * mDensity);

            int rotate = dob.rotate;

            float scale = 1f; //dob.scale;
            float alpha = 1f; //dob.alpha;

            if (tr == null && mSingleTransition != null) {
                tr = mSingleTransition;
            }

            if (tr != null) {
                tr.beforeDraw(getContext(), width * fitScale, height * fitScale, originWidth, originHeight, leftShift, topShift, mDensity, fitScale);
                left = tr.xTransform(this, dob, left, tr.mCurrentPercent, mDensity);
                top = tr.yTransform(this, dob, top, tr.mCurrentPercent, mDensity);
                rotate = tr.rotateTransform(this, dob, tr.mCurrentPercent);
                alpha = tr.alphaTransform(this, dob, tr.mCurrentPercent);
                scale = tr.scaleTransform(this, dob, tr.mCurrentPercent);
            }

            int cx = left + (dob.getWidth() >> 1);
            int cy = top + (dob.getHeight() >> 1);

            alpha = Math.min(alpha, 1f);

            canvas.save();
            canvas.scale(scale, scale, cx, cy);
            canvas.rotate(rotate, cx, cy);
            dob.draw(canvas, left, top, scale, alpha);
            canvas.restore();


            if (DEBUG && tr != null) {
                Paint paint = new Paint();
                paint.setColor(Color.GREEN);

                canvas.drawCircle(tr.getX() * mDensity, tr.getY() * mDensity, 10, paint);

                paint.setStrokeWidth(2);
                canvas.drawLine(mOriginRect.width() / 2, 0, mOriginRect.width() / 2, height, paint);
                canvas.drawLine(0, mOriginRect.height() / 2, mOriginRect.width(), mOriginRect.height() / 2, paint);

            }
        }

        canvas.restore();
        if (DEBUG) {
            Paint paint = new Paint();
            paint.setColor(Color.GREEN);

            paint.setStrokeWidth(2);
            canvas.drawLine(width / 2, 0, width / 2, height, paint);
            canvas.drawLine(0, height / 2, width, height / 2, paint);

        }
    }

    public void setRequestedBottomPadding(int requestedBottomPadding) {
        mRequestedBottomPadding = requestedBottomPadding;
    }

    public void addObject(DynamicObject object) {
        if (!mObjects.containsKey(object.id)) {
            object.prepare(getContext(), mOriginRect);
            mObjects.put(object.id, object);
        }
    }

    public void removeObject(DynamicObject object) {
        removeObject(object.id);
    }

    public void removeObject(int id) {
        mObjects.remove(id);
    }

    public void addTransition(DynamicTransition transition) {
        addTransition(transition.id, transition);
    }

    public void addTransition(int id, DynamicTransition transition) {
        transition.prepare(getContext(), mOriginRect);
        mTransitions.put(id, transition);
    }

    public void setSingleTransition(DynamicTransition transition) {
        mSingleTransition = transition;
    }

    public void clearTransitions() {
        mTransitions.clear();
    }

    private AnimatorSet set;

    private boolean isVisible = false;

    private long DURATION = 800;

    public float cancelJump() {
        if (set != null) {
            set.cancel();
            return mTransitionPercent;
        }
        return 0f;
    }

    public void jump(boolean visible, final Runnable endRunnable, boolean withOvershoot) {

        isVisible = visible;

        if (set != null) {
            set.cancel();
        }

        final long duration = mTransitionPercent > 0 && mTransitionPercent != 1f ?
                (long) (visible ? (DURATION * (1f - mTransitionPercent)) : (DURATION * mTransitionPercent)) : DURATION;

        set = new AnimatorSet();

        ArrayList<Animator> mAnimators = new ArrayList<>();
        ValueAnimator animatorWithLongestDuration = null;

        for (final DynamicTransition transition : mTransitions.values()) {
            ValueAnimator animator = ValueAnimator.ofFloat(mTransitionPercent, visible ? 1f : 0f);
            animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator valueAnimator) {
                    transition.setCurrentPercent((float) valueAnimator.getAnimatedValue());
                }
            });
            animator.setDuration((long) (duration / transition.speedFactor));
            if (transition.interpolator != null) {
                animator.setInterpolator(transition.interpolator);
            }
            mAnimators.add(animator);
            if (animatorWithLongestDuration == null || animator.getDuration() > animatorWithLongestDuration.getDuration()) {
                animatorWithLongestDuration = animator;
            }
        }

        if (mSingleTransition != null) {
            ValueAnimator animator = ValueAnimator.ofFloat(mTransitionPercent, visible ? 1f : 0f);
            animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator valueAnimator) {
                    if (mSingleTransition != null) {
                        mSingleTransition.setCurrentPercent(mTransitionPercent);
                    }
                }
            });
            animator.setDuration((long) (duration / mSingleTransition.speedFactor));
            if (mSingleTransition != null) {
                animator.setInterpolator(mSingleTransition.interpolator);
            }
            mAnimators.add(animator);
            if (animatorWithLongestDuration == null || animator.getDuration() > animatorWithLongestDuration.getDuration()) {
                animatorWithLongestDuration = animator;
            }
        }

        if (animatorWithLongestDuration != null) {
            animatorWithLongestDuration.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator valueAnimator) {
                    onTransitionPercentChange((float) valueAnimator.getAnimatedValue());
                    invalidate();
                }
            });
        }

        if (withOvershoot) {
            set.setInterpolator(new OvershootInterpolator(1.3f));
        }
        set.playTogether(mAnimators);
        set.addListener(new AnimatorListenerAdapter() {
            private boolean isCanceled = false;

            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                if (!isCanceled) {
                    set = null;
                    if (endRunnable != null) {
                        endRunnable.run();
                    }
                }
            }

            @Override
            public void onAnimationCancel(Animator animation) {
                super.onAnimationCancel(animation);
                isCanceled = true;
            }
        });
        set.start();
    }

    public boolean isJumping() {
        return set != null;
    }

    public void setTransitionPercent(float percent) {
        Log.e(TAG, "percent: " + percent);
        onTransitionPercentChange(percent);
        for (DynamicTransition tr : mTransitions.values()) {
            if (tr.interpolator != null) {
                tr.setCurrentPercent(tr.interpolator.getInterpolation((Math.min(1f, percent * tr.speedFactor))));
            } else {
                tr.setCurrentPercent(Math.min(1f, percent * tr.speedFactor));
            }
        }
        if (mSingleTransition != null) {
            mSingleTransition.setCurrentPercent(Math.min(1f, percent * mSingleTransition.speedFactor));
        }
        invalidate();
    }

    private void onTransitionPercentChange(float percent) {
        mTransitionPercent = percent;
        if (mTransitionUpdateListener != null) {
            mTransitionUpdateListener.onTransitionUpdate(mTransitionPercent);
        }
    }

    public static abstract class DynamicObject {

        int id;

        float x = 0;
        float y = 0;

        int rotate = 0;

        float scale = 1f;
        float alpha = 1f;

        public DynamicObject(int id) {
            this.id = id;
        }

        protected void prepare(Context context, RectF rect) {
        }

        abstract void draw(Canvas canvas, int left, int top, float scale, float alpha);

        abstract int getWidth();

        abstract int getHeight();

        public <T extends DynamicObject> T x(float x) {
            this.x = x;
            return (T) this;
        }

        public <T extends DynamicObject> T y(float y) {
            this.y = y;
            return (T) this;
        }

        public <T extends DynamicObject> T rotate(int rotate) {
            this.rotate = rotate;
            return (T) this;
        }

        public <T extends DynamicObject> T scale(float scale) {
            this.scale = scale;
            return (T) this;
        }

        public <T extends DynamicObject> T alpha(float alpha) {
            this.alpha = alpha;
            return (T) this;
        }

        public float getX() {
            return x;
        }

        public float getY() {
            return y;
        }

        public int getRotate() {
            return rotate;
        }

        public float getScale() {
            return scale;
        }

        public float getAlpha() {
            return alpha;
        }
    }

    public static abstract class DynamicTransition {

        int id;

        private Interpolator interpolator;

        public DynamicTransition(int id) {
            this.id = id;
        }

        protected void prepare(Context context, RectF rect) {
        }

        protected void beforeDraw(Context context, float viewWidth, float viewHeight, float rectWidth, float rectHeight, float leftShift, float topShift, float density, float scaleDown) {
        }

        abstract int xTransform(DynamicView view, DynamicObject dob, int x, float percent, float density);

        abstract int yTransform(DynamicView view, DynamicObject dob, int y, float percent, float density);

        abstract int rotateTransform(DynamicView view, DynamicObject dob, float percent);

        abstract float scaleTransform(DynamicView view, DynamicObject dob, float percent);

        abstract float alphaTransform(DynamicView view, DynamicObject dob, float percent);

        int getX() {
            return 0;
        }

        int getY() {
            return 0;
        }

        private float mCurrentPercent = 0f;
        private float speedFactor = 1f;

        public DynamicTransition speedFactor(float factor) {
            speedFactor = factor;
            return this;
        }

        public void setCurrentPercent(float mCurrentPercent) {
            this.mCurrentPercent = mCurrentPercent;
        }

        public DynamicTransition interpolator(Interpolator interpolator) {
            this.interpolator = interpolator;
            return this;
        }

    }

    public interface OnTransitionUpdateListener {
        void onTransitionUpdate(float percent);
    }

}