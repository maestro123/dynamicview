package maestro.dyv.dynamic;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by maestro123 on 22.12.2016.
 */

public class DynamicLayout extends ViewGroup {

    public static final String TAG = DynamicLayout.class.getSimpleName();

    private static final int SWIPE_MIN_DISTANCE = 100;
    private static final int SWIPE_MAX_OFF_PATH = 300;
    private static final int SWIPE_THRESHOLD_VELOCITY = 180;

    private TextView mTitleView;
    private DynamicView mDynamicView;

    private GestureDetector mDetector;
    private PrepareListener mPrepareListener;
    private DynamicListener mDynamicListener;

    private Direction mDirection;

    private int mCurrentScene = 0;
    private int mPendingScene = -1;

    private float mScrollPercent = 0f;

    private boolean isFling = false;
    private boolean isScrolling = false;

    private int mMinSwipeDistance;
    private int mMaxSwipeDistance;
    private int mMinThreshHoldVelocity;

    private Drawable mDynamicBackground;

    private enum Direction {
        None, Start, End;
    }

    public DynamicLayout(Context context) {
        this(context, null);
    }

    public DynamicLayout(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public DynamicLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        mDetector = new GestureDetector(context, mGestureListener);

        final DisplayMetrics metrics = getResources().getDisplayMetrics();

        mMinSwipeDistance = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, SWIPE_MIN_DISTANCE, metrics);
        mMaxSwipeDistance = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, SWIPE_MAX_OFF_PATH, metrics);
        mMinThreshHoldVelocity = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, SWIPE_THRESHOLD_VELOCITY, metrics);

    }

    public void setDynamicBackground(DynamicBackground dynamicBackground) {
        if (mDynamicBackground != null) {
            mDynamicBackground.setCallback(null);
        }
        mDynamicBackground = (Drawable) dynamicBackground;
        if (mDynamicBackground != null) {
            mDynamicBackground.setCallback(this);
            setWillNotDraw(false);
        }
    }

    @Override
    protected boolean verifyDrawable(Drawable who) {
        return who == mDynamicBackground || super.verifyDrawable(who);
    }

    @Override
    public void invalidateDrawable(Drawable drawable) {
        super.invalidateDrawable(drawable);
    }

    @Override
    protected void dispatchDraw(Canvas canvas) {
        super.dispatchDraw(canvas);

        if (mDynamicBackground != null) {
            mDynamicBackground.setBounds(getPaddingLeft(), getPaddingTop(), getWidth(), getHeight());
            mDynamicBackground.draw(canvas);
        }
    }

    public void setPrepareListener(PrepareListener listener) {
        mPrepareListener = listener;

        mCurrentScene = 0;
        mPrepareListener.prepareForState(mDynamicView, 0);
        if (mTitleView != null) {
            mCurrentAppliedTitleSection = mCurrentScene;
            mTitleView.setText(mPrepareListener.getTitle(mCurrentScene));
        }
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        for (int i = 0; i < getChildCount(); i++) {
            final View child = getChildAt(i);
            if (child instanceof DynamicView) {
                mDynamicView = (DynamicView) getChildAt(i);
                mDynamicView.setOnTransitionUpdateListener(new DynamicView.OnTransitionUpdateListener() {
                    @Override
                    public void onTransitionUpdate(float percent) {
                        if (isScrolling && mDirection != Direction.None) {
                            mScrollPercent = mDirection == Direction.Start ? -percent : percent;
                            dispatchScroll();
                        }
                    }
                });
            } else if (child instanceof TextView && child.getId() == android.R.id.title) {
                mTitleView = (TextView) child;
            }
        }
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {

        int width = getWidth();
        int height = getHeight();

        int topOffset = getPaddingTop();

        if (mTitleView != null && !isTitleExternal) {
            int childWidth = mTitleView.getMeasuredWidth();
            int childHeight = mTitleView.getMeasuredHeight();

            int childLeft = getPaddingLeft() + (width - getPaddingLeft() - getPaddingRight() - childWidth) / 2;
            int childTop = topOffset;

            mTitleView.layout(childLeft, childTop, childLeft + childWidth, childTop + childHeight);
        }

        if (mDynamicView != null) {

            int childWidth = mDynamicView.getMeasuredWidth();
            int childHeight = mDynamicView.getMeasuredHeight();

            int childLeft = getPaddingLeft() + (width - getPaddingLeft() - getPaddingRight() - childWidth) / 2;
            int childTop = topOffset + (height - topOffset - getPaddingBottom() - childHeight) / 2;

            mDynamicView.layout(childLeft, childTop, childLeft + childWidth, childTop + childHeight);
        }

    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int topOffset = 0;
        if (mTitleView != null && !isTitleExternal) {
            measureChild(mTitleView, widthMeasureSpec, heightMeasureSpec);
            topOffset += mTitleView.getMeasuredHeight();
        }
        if (mDynamicView != null) {
            mDynamicView.setPadding(0, topOffset, 0, 0);
            if (getBackground() instanceof DynamicBackground) {
                mDynamicView.setRequestedBottomPadding(getBackground().getIntrinsicHeight());
            }
            measureChild(mDynamicView, widthMeasureSpec, heightMeasureSpec);
        }
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        if (mDynamicView.isJumping()) {
            return false;
        }
        mDetector.onTouchEvent(ev);
        return isScrolling;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (mDynamicView.isJumping()) {
            return false;
        }
        mDetector.onTouchEvent(event);

        if (event.getAction() == MotionEvent.ACTION_UP) {
            boolean jump =
                    mPendingScene != -1
                            && mPendingScene != mCurrentScene
                            && ((mScrollPercent >= 0.5f && mDirection == Direction.End)
                            || (mScrollPercent <= -0.5f && mDirection == Direction.Start)
                            || isFling);
            if (jump) {
                if (mDirection == Direction.End) {
                    mPendingScene = mCurrentScene + 1;
                    mDynamicView.jump(true, mJumpRunnable, true);
                } else {
                    mPendingScene = mCurrentScene - 1;
                    mDynamicView.jump(false, mJumpRunnable, true);
                }
            } else if (mDirection != Direction.None) {
                mPendingScene = -1;
                mDynamicView.jump(mDirection == Direction.Start, mJumpRunnable, false);
            }
        } else if (event.getAction() == MotionEvent.ACTION_CANCEL) {
            mPendingScene = -1;
            mDynamicView.jump(false, mJumpRunnable, false);
        }
        return true;
    }

    private boolean isTitleExternal = false;

    public void setTitleView(TextView textView) {
        mTitleView = textView;
        isTitleExternal = true;
        if (mPrepareListener != null && mTitleView != null) {
            mTitleView.setText(mPrepareListener.getTitle(mCurrentScene));
        }
    }

    private int mCurrentAppliedTitleSection;

    private void dispatchScroll() {
        if (isScrolling) {
            if (mTitleView != null) {
                float percent = Math.abs(Math.max(-1f, Math.min(1f, mScrollPercent))) * 2f;
                int affectSection = mCurrentScene;
                if (percent >= 1f) {
                    if (mDirection == Direction.End && mPendingScene != -1) {
                        affectSection = mPendingScene;
                    }
                    mTitleView.setAlpha(percent - 1f);
                } else {
                    if (mDirection == Direction.Start && mPendingScene != -1) {
                        affectSection = mPendingScene;
                    }
                    mTitleView.setAlpha(1f - percent);
                }
                if (mCurrentAppliedTitleSection != affectSection) {
                    mCurrentAppliedTitleSection = affectSection;
                    mTitleView.setText(mPrepareListener.getTitle(affectSection));
                }
            }
            if (mDynamicBackground != null && mDynamicBackground instanceof DynamicBackground) {
                ((DynamicBackground) mDynamicBackground).setWidthOffset((int) (getWidth() * (Math.abs(mScrollPercent) + (mDirection == Direction.Start ? mCurrentScene - 1 : mCurrentScene))));
            }
        }
    }

    private final Runnable mJumpRunnable = new Runnable() {
        @Override
        public void run() {
            if (mPendingScene != -1) {
                mCurrentScene = mPendingScene;
            }
            isScrolling = false;
            mPrepareListener.prepareForState(mDynamicView, mCurrentScene);
            mDynamicView.setTransitionPercent(1f);
        }
    };

    private boolean trackScroll(float percent) {
        if ((mScrollPercent < 0 && percent >= 0) || (mScrollPercent >= 0 && percent < 0)) {
            mPendingScene = -1;
            mPrepareListener.prepareForState(mDynamicView, mCurrentScene);
        }
        if (percent < 0) {
            if (mCurrentScene == 0) {
                mDirection = Direction.None;
                mScrollPercent = 0f;
            } else {
                mDirection = Direction.Start;
                mScrollPercent = percent;
                mPendingScene = mCurrentScene - 1;
            }
        } else {
            if (mCurrentScene >= mPrepareListener.getStateCount() - 1) {
                mDirection = Direction.None;
                mScrollPercent = 0f;
            } else {
                if (mPendingScene == -1) {
                    mPendingScene = mCurrentScene + 1;
                    mPrepareListener.prepareForState(mDynamicView, mPendingScene);
                }
                mDirection = Direction.End;
                mScrollPercent = percent;
            }
        }
        if (mScrollPercent > 1f) {
            mScrollPercent = 1f;
        } else if (mScrollPercent < -1f) {
            mScrollPercent = -1f;
        }
        Log.e(TAG, "mScrollPercent: " + mScrollPercent);
        mDynamicView.setTransitionPercent(mDirection == Direction.None ? 1f : getTransitionPercent());
        return mDirection != Direction.None;
    }

    private float getTransitionPercent() {
        return Math.abs(mScrollPercent < 0 ? 1f + mScrollPercent : mScrollPercent);
    }

    private final GestureDetector.OnGestureListener mGestureListener = new GestureDetector.SimpleOnGestureListener() {

        float xScroll = 0;

        @Override
        public boolean onDown(MotionEvent e) {
            mDirection = Direction.None;
            isScrolling = false;
            mPendingScene = -1;
            isFling = false;
            xScroll = 0;
            return super.onDown(e);
        }

        @Override
        public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
            if (!isScrolling) {
                isScrolling = true;
                xScroll = getWidth() * mDynamicView.cancelJump();
            } else if (trackScroll((xScroll + distanceX) / getWidth())) {
                xScroll += distanceX;
//                dispatchScroll();
            }
            return true;
        }

        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            if ((Math.abs(e1.getY() - e2.getY()) > mMaxSwipeDistance
                    && Math.abs(e1.getX() - e2.getX()) > mMaxSwipeDistance)
                    || (Math.abs(velocityX) < mMinThreshHoldVelocity
                    && Math.abs(velocityY) < mMinThreshHoldVelocity)) {
                return false;
            }
            if (e1.getX() - e2.getX() < mMinSwipeDistance
                    && e2.getX() - e1.getX() < mMinSwipeDistance
                    && e1.getY() - e2.getY() < mMinSwipeDistance
                    && e2.getY() - e1.getY() < mMinSwipeDistance) {
                return false;
            }

            Direction direction = e1.getX() < e2.getX() ? Direction.Start : Direction.End;

            assert mPrepareListener != null : "Null prepare listener!";
            if (direction == Direction.End && mCurrentScene >= mPrepareListener.getStateCount() - 1) {
                return false;
            }
            if (direction == Direction.Start && mCurrentScene == 0) {
                return false;
            }

            isFling = true;
            mDirection = direction;

            return true;
        }
    };

    public interface PrepareListener {
        int getStateCount();

        void prepareForState(DynamicView dynamicView, int state);

        CharSequence getTitle(int section);
    }

    public interface DynamicListener {
        void onScroll(float percent);
    }

    public interface DynamicBackground {
        void setWidthOffset(int offset);
    }

}
