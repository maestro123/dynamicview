package maestro.dyv.dynamic;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.support.v7.graphics.drawable.DrawableWrapper;

/**
 * Created by maestro123 on 23.12.2016.
 */

public class DynamicLayoutBackground extends DrawableWrapper implements DynamicLayout.DynamicBackground {

    public static final String TAG = DynamicLayoutBackground.class.getSimpleName();

    private int mWidthOffset = 0;

    public DynamicLayoutBackground(Drawable drawable) {
        super(drawable);
    }

    @Override
    public void draw(Canvas canvas) {
        Rect bounds = getBounds();

        Drawable drawable = getWrappedDrawable();
        int dw = drawable.getIntrinsicWidth();
        int dh = drawable.getIntrinsicHeight();

        Callback callback = drawable.getCallback();
        drawable.setCallback(null);

        int verticalShift = 0;
        int horizontalShift = 0;
        for (int i = 0; i < 3; i++) {
//            float alpha = i == 0 ? 0.2f : i == 1 ? 0.3f : 1f;
            float alpha = 1f;
            float requiredOffsetPercent = 0.2f * (i + 1);
            verticalShift = (int) (dh * (i == 0 ? 0f : i == 1 ? 0.2f : 0.4f));
            int fillColor = Color.parseColor(i == 0 ? "#39CCCC" : i == 1 ? "#87E0E0" : "#FFFFFF");
            drawable.setColorFilter(fillColor, PorterDuff.Mode.SRC_IN);
            int scrollOffset = (int) (mWidthOffset * requiredOffsetPercent);
            if (scrollOffset > bounds.width()) {
                scrollOffset -= scrollOffset / bounds.width() * bounds.width();
            }
            int left = bounds.centerX() - dw / 2 + horizontalShift + scrollOffset;
            int top = bounds.bottom - dh + verticalShift;
            drawable.setAlpha((int) (255 * alpha));
            while (left < bounds.right) {
                drawable.setBounds(left, top, left + dw, top + dh);
                drawable.draw(canvas);
                left += dw;
            }
            int right = bounds.centerX() - dw / 2 + horizontalShift + scrollOffset;
            while (right > 0) {
                drawable.setBounds(right - dw, top, right, top + dh);
                drawable.draw(canvas);
                right -= dw;
            }
            verticalShift += dh * 0.3f;
            horizontalShift += dw * 0.05f;
        }

        drawable.setCallback(callback);
    }

    @Override
    public void setWidthOffset(int offset) {
        mWidthOffset = offset * -1;
        invalidateSelf();
    }

}
