package maestro.dyv.dynamic;

import android.content.Context;

/**
 * Created by maestro123 on 22.12.2016.
 */

public class SimpleDynamicTransition extends DynamicView.DynamicTransition {

    public static final String TAG = SimpleDynamicTransition.class.getSimpleName();

    private int xStart = Integer.MAX_VALUE;
    private int yStart = Integer.MAX_VALUE;

    private int xTarget = Integer.MAX_VALUE;
    private int yTarget = Integer.MAX_VALUE;

    private int rotateStart = Integer.MAX_VALUE;
    private int rotateTarget = Integer.MAX_VALUE;

    private float scaleStart = Float.MAX_VALUE;
    private float scaleTarget = Float.MAX_VALUE;

    private float alphaStart = Float.MAX_VALUE;
    private float alphaTarget = Float.MAX_VALUE;

    private boolean isStartIsRelative = false;

    private OutOffScreenSide xOutOfScreenStart = OutOffScreenSide.None;
    private OutOffScreenSide yOutOfScreenStart = OutOffScreenSide.None;

    private OutOffScreenSide xOutOfScreenTarget = OutOffScreenSide.None;
    private OutOffScreenSide yOutOfScreenTarget = OutOffScreenSide.None;

    public enum OutOffScreenSide {
        Start, End, None
    }

    public SimpleDynamicTransition(int id) {
        super(id);
    }

    @Override
    protected void beforeDraw(Context context, float viewWidth, float viewHeight, float rectWidth, float rectHeight, float leftShift, float topShift, float density, float fitScale) {
        super.beforeDraw(context, viewWidth, viewHeight, rectWidth, rectHeight, leftShift, topShift, density, fitScale);

        if (xOutOfScreenStart != OutOffScreenSide.None || xOutOfScreenTarget != OutOffScreenSide.None) {
            float horizontalOffset = rectWidth / density + leftShift / density;
            xStart = resolveOutOfScreen(xOutOfScreenStart, xStart, -horizontalOffset, horizontalOffset);
            xTarget = resolveOutOfScreen(xOutOfScreenTarget, xTarget, -horizontalOffset, horizontalOffset);
        }
        if (yOutOfScreenStart != OutOffScreenSide.None || yOutOfScreenTarget != OutOffScreenSide.None) {
            float verticalOffset = rectHeight / density + topShift / density;
            yStart = resolveOutOfScreen(yOutOfScreenStart, yStart, -verticalOffset, verticalOffset);
            yTarget = resolveOutOfScreen(yOutOfScreenTarget, yTarget, -verticalOffset, verticalOffset);
        }
    }

    @Override
    int xTransform(DynamicView view, DynamicView.DynamicObject dob, int x, float percent, float density) {
        return computeValue(x, adjustPercent(percent), xStart, xTarget, isStartIsRelative ? dob.getWidth() >> 1 : 0, density);
    }

    @Override
    int yTransform(DynamicView view, DynamicView.DynamicObject dob, int y, float percent, float density) {
        return computeValue(y, adjustPercent(percent), yStart, yTarget, isStartIsRelative ? dob.getHeight() >> 1 : 0, density);
    }

    @Override
    int rotateTransform(DynamicView view, DynamicView.DynamicObject dob, float percent) {
        return computeValue(dob.rotate, adjustPercent(percent), rotateStart, rotateTarget);
    }

    @Override
    float alphaTransform(DynamicView view, DynamicView.DynamicObject dob, float percent) {
        return Math.max(0, Math.min(255, computeValue(dob.alpha, adjustPercent(percent), alphaStart, alphaTarget)));
    }

    @Override
    float scaleTransform(DynamicView view, DynamicView.DynamicObject dob, float percent) {
        return computeValue(dob.scale, adjustPercent(percent), scaleStart, scaleTarget);
    }

    private int resolveOutOfScreen(OutOffScreenSide side, int value, float sizeStart, float sizeEnd) {
        return side == OutOffScreenSide.None ? value : (int) (side == OutOffScreenSide.Start ? sizeStart : sizeEnd);
//        return side == OutOffScreenSide.None ? value : (int) (resolveValue(value) + (side == OutOffScreenSide.Start ? sizeStart : sizeEnd));
    }

    private int resolveValue(int value) {
        return value == Integer.MAX_VALUE ? 0 : value;
    }

    public SimpleDynamicTransition xOutOfScreenStart(OutOffScreenSide side) {
        xOutOfScreenStart = side;
        return this;
    }

    public SimpleDynamicTransition xOutOfScreenTarget(OutOffScreenSide side) {
        xOutOfScreenTarget = side;
        return this;
    }

    public SimpleDynamicTransition yOutOfScreenStart(OutOffScreenSide side) {
        yOutOfScreenStart = side;
        return this;
    }

    public SimpleDynamicTransition yOutOfScreenTarget(OutOffScreenSide side) {
        yOutOfScreenTarget = side;
        return this;
    }

    public SimpleDynamicTransition xStart(int x) {
        xStart = x;
        return this;
    }

    public SimpleDynamicTransition xTarget(int x) {
        xTarget = x;
        return this;
    }

    public SimpleDynamicTransition yStart(int y) {
        yStart = y;
        return this;
    }

    public SimpleDynamicTransition yTarget(int y) {
        yTarget = y;
        return this;
    }

    public SimpleDynamicTransition rotateStart(int rotate) {
        rotateStart = rotate;
        return this;
    }

    public SimpleDynamicTransition rotateTarget(int rotate) {
        rotateTarget = rotate;
        return this;
    }

    public SimpleDynamicTransition scaleStart(float scale) {
        scaleStart = scale;
        return this;
    }

    public SimpleDynamicTransition scaleTarget(float scale) {
        scaleTarget = scale;
        return this;
    }

    public SimpleDynamicTransition alphaStart(float alpha) {
        alphaStart = alpha;
        return this;
    }

    public SimpleDynamicTransition alphaTarget(float alpha) {
        alphaTarget = alpha;
        return this;
    }

    public SimpleDynamicTransition startIsRelative(boolean relative) {
        isStartIsRelative = relative;
        return this;
    }

    private static int computeValue(int dobValue, float percent, int start, int target) {
        if (!isIntValuesSet(start) && !isIntValuesSet(target)) {
            return dobValue;
        }
        if (!isIntValuesSet(start)) {
            start = dobValue;
        }
        if (!isIntValuesSet(target)) {
            target = dobValue;
        }
        return (int) (start + (target - start) * percent);
    }

    private static int computeValue(int dobValue, float percent, int start, int target, int valueOffset, float density) {
        if (!isIntValuesSet(start) && !isIntValuesSet(target)) {
            return dobValue;
        }
        if (!isIntValuesSet(start)) {
            start = dobValue;
        } else {
            start = (int) (start * density - valueOffset);
        }
        if (!isIntValuesSet(target)) {
            target = dobValue;
        } else {
            target = (int) (target * density - valueOffset);
        }
        return (int) (start + (target - start) * percent);
    }

    private static float computeValue(float dobValue, float percent, float start, float target) {
        if (!isFloatValueSet(start) && !isFloatValueSet(target)) {
            return dobValue;
        }
        if (!isFloatValueSet(start)) {
            start = dobValue;
        } else if (!isFloatValueSet(target)) {
            target = dobValue;
        }
        return start + (target - start) * percent;
    }

    @Override
    int getX() {
        return xTarget;
    }

    @Override
    int getY() {
        return yTarget;
    }

    private float adjustPercent(float percent) {
//        return Math.min(percent * speedFactor, 1f);
        return percent;// * speedFactor;
    }

    private static boolean isIntValuesSet(int value) {
        return value != Integer.MAX_VALUE;
    }

    private static boolean isFloatValueSet(float value) {
        return value != Float.MAX_VALUE;
    }
}