package maestro.dyv.dynamic;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.TypedValue;
import android.view.View;

import java.lang.ref.WeakReference;

/**
 * Created by maestro123 on 27.12.2016.
 */

public class ArcBackground extends Drawable implements DynamicLayout.DynamicBackground {

    private final Paint mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);

    private float radius = 0;
    private WeakReference<View> mView;

    private float[] RADIUS = new float[]{32}; //new float[]{50, 64, 48, 62, 90, 48, 28, 64, 96};

    private Rect mRect = new Rect();

    private float density;

    public ArcBackground(Context context, View view) {
        radius = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 100, context.getResources().getDisplayMetrics());
        mView = new WeakReference<View>(view);
        density = context.getResources().getDisplayMetrics().density;
    }

    @Override
    public void draw(Canvas canvas) {
        final View anchor = mView.get();
        if (anchor == null) {
            return;
        }

        final Rect bounds = getBounds();
        final Rect anchorBounds = new Rect();
        anchor.getGlobalVisibleRect(anchorBounds);

        int xy[] = new int[2];
        xy[0] = anchor.getLeft();
        xy[1] = anchor.getTop();

        mPaint.setStyle(Paint.Style.FILL);
        mPaint.setColor(Color.WHITE);

        double a = anchor.getMeasuredWidth() * (300f / 280f);
        double b = anchor.getMeasuredHeight() * (400f / 128f);
        double c = (int) Math.sqrt(Math.pow(a, 2) + Math.pow(b, 2));
        double p = (a + b + c) / 2;
        double R = (a * b * c) / (4 * Math.sqrt(p * (p - a) * (p - b) * (p - c)));

        float cx = (float) (a / 2);
        float cy = (float) Math.sqrt(Math.pow(R, 2) - Math.pow(cx, 2));

        cx += xy[0];
        cy += xy[1];

        cx *= 1.05f;
        cy *= 1.05f;

        canvas.drawCircle(cx, cy, (float) R, mPaint);

        mPaint.setColor(Color.WHITE);

        int startDegrees = 270;

        float x = 0;
        float y = 0;
        int count = 0;
        boolean first = true;
        do {
            float radius = RADIUS[count] * density;
            startDegrees = (int) (startDegrees + (!first ? (int) (radius * 0.15) : 0));
            x = (float) (R * Math.cos(Math.toRadians(startDegrees)) + cx);
            y = (float) (R * Math.sin(Math.toRadians(startDegrees)) + cy);
            canvas.drawCircle(x, y, radius, mPaint);
            count++;
            first = false;
            if (count == RADIUS.length) {
                count = 0;
            }
        } while (x < bounds.right);

        x = 0;
        y = 0;
        count = RADIUS.length - 1;
        first = true;
        do {
            float radius = RADIUS[count] * density;
            startDegrees = (int) (startDegrees - (!first ? (int) (radius * 0.15) : 0));
            x = (float) (R * Math.cos(Math.toRadians(startDegrees)) + cx);
            y = (float) (R * Math.sin(Math.toRadians(startDegrees)) + cy);
            canvas.drawCircle(x, y, radius, mPaint);
            count--;
            first = false;
            if (count < 0) {
                count = RADIUS.length - 1;
            }
        } while (y < bounds.bottom);
    }

    @Override
    public void setAlpha(int i) {
        mPaint.setAlpha(i);
    }

    @Override
    public void setColorFilter(ColorFilter colorFilter) {
        mPaint.setColorFilter(colorFilter);
    }

    @Override
    public int getOpacity() {
        return PixelFormat.OPAQUE;
    }

    @Override
    public void setWidthOffset(int offset) {
        //ignore
    }
}
