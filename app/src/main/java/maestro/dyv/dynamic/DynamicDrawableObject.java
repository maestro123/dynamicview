package maestro.dyv.dynamic;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;

/**
 * Created by maestro123 on 22.12.2016.
 */

public class DynamicDrawableObject extends DynamicView.DynamicObject {

    private Drawable mDrawable;
    private int mDrawableResource;

    public DynamicDrawableObject(int id, Drawable drawable) {
        super(id);
        mDrawable = drawable;
    }

    public DynamicDrawableObject(int id, int drawableResource) {
        super(id);
        mDrawableResource = drawableResource;
    }


    public DynamicDrawableObject(int drawableResource) {
        super(drawableResource);
        mDrawableResource = drawableResource;
    }

    @Override
    protected void prepare(Context context, RectF rect) {
        super.prepare(context, rect);
        if (mDrawableResource > 0) {
            mDrawable = ContextCompat.getDrawable(context, mDrawableResource);
        }
    }

    @Override
    void draw(Canvas canvas, int left, int top, float scale, float alpha) {
        mDrawable.setBounds(left, top, left + getWidth(), top + getHeight());
        mDrawable.setAlpha((int) (255 * alpha * getAlpha()));
        mDrawable.draw(canvas);
    }

    @Override
    int getWidth() {
        return (int) (mDrawable.getIntrinsicWidth() * getScale());
    }

    @Override
    int getHeight() {
        return (int) (mDrawable.getIntrinsicHeight() * getScale());
    }
}
