package maestro.dyv;

import android.content.res.Configuration;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import maestro.dyv.dynamic.ArcBackground;
import maestro.dyv.dynamic.DynamicDrawableObject;
import maestro.dyv.dynamic.DynamicLayout;
import maestro.dyv.dynamic.DynamicLayoutBackground;
import maestro.dyv.dynamic.DynamicView;
import maestro.dyv.dynamic.SimpleDynamicTransition;

public class MainActivity extends AppCompatActivity {

    public static final String TAG = MainActivity.class.getSimpleName();

    DynamicDrawableObject mWordObject;
    DynamicDrawableObject mVideoObject;
    DynamicDrawableObject mGraphicObject;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final DynamicLayout dynamicLayout = (DynamicLayout) findViewById(R.id.dynamic_layout);
        dynamicLayout.setPrepareListener(mPrepareListener);

        Drawable dTest = new TilingDrawable(this, R.drawable.bg_primary_pattern);
        dynamicLayout.setBackground(dTest);

        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            DynamicLayoutBackground background = new DynamicLayoutBackground(getResources().getDrawable(R.drawable.ptrn_clouds_prelogin));
            dynamicLayout.setDynamicBackground(background);
        } else {
            View testParent = findViewById(R.id.test_parent);
            ArcBackground background = new ArcBackground(this, testParent);
            dynamicLayout.setDynamicBackground(background);
            dynamicLayout.setTitleView((TextView) findViewById(android.R.id.title));
        }

        final DynamicView dynamicView = (DynamicView) findViewById(R.id.dynamic_view);

        dynamicView.addObject(new DynamicDrawableObject(R.drawable.ic_box_158x120).x(81).y(243));
        dynamicView.addObject(new DynamicDrawableObject(R.drawable.ic_light).x(20.5f).y(142));
        dynamicView.addObject(new DynamicDrawableObject(R.drawable.ic_text_file_24x34).x(180).y(237).rotate(40));
        dynamicView.addObject(new DynamicDrawableObject(R.drawable.ic_music_22x30).x(152).y(252));
        dynamicView.addObject(new DynamicDrawableObject(R.drawable.ic_graphic_26x36).x(120).y(235).rotate(-27));
        dynamicView.addObject(new DynamicDrawableObject(R.drawable.ic_text_file_50x64).x(100).y(153).rotate(-6));
        dynamicView.addObject(new DynamicDrawableObject(R.drawable.ic_offline_28).x(290).y(114).rotate(45));
        dynamicView.addObject(new DynamicDrawableObject(R.drawable.ic_upload_40x28).x(195).y(27).rotate(14));
        dynamicView.addObject(new DynamicDrawableObject(R.drawable.ic_plane_40x26).x(150).y(53).rotate(-3));
        dynamicView.addObject(new DynamicDrawableObject(R.drawable.ic_share_22x24).x(150).y(20).rotate(-20));
        dynamicView.addObject(new DynamicDrawableObject(R.drawable.ic_lock_14x18).x(62).y(60).rotate(-20));
        dynamicView.addObject(new DynamicDrawableObject(R.drawable.ic_favorite_36).x(16).y(82).rotate(-34));
        dynamicView.addObject(new DynamicDrawableObject(R.drawable.ic_music_36x48).x(270).y(170).rotate(30));

        dynamicView.addObject(new DynamicDrawableObject(R.drawable.ic_10_gb_174x136).x(74).y(85));
        dynamicView.addObject(new DynamicDrawableObject(R.drawable.ic_free_90x30).x(164).y(181).rotate(7));
        dynamicView.addObject(new DynamicDrawableObject(R.drawable.ic_gb_96x65).x(186).y(98).rotate(-6));

        dynamicView.addObject(mGraphicObject = new DynamicDrawableObject(R.drawable.ic_graphic).x(245).y(32).rotate(30).scale(0.6388f));
        dynamicView.addObject(mVideoObject = new DynamicDrawableObject(R.drawable.ic_video).x(18).y(142).rotate(-40).scale(0.6f));
        dynamicView.addObject(mWordObject = new DynamicDrawableObject(R.drawable.ic_word).x(78).y(0).rotate(-15).scale(0.5362f));

        /* initial transition*/

        SimpleDynamicTransition transition = (SimpleDynamicTransition) defFirstTransition().speedFactor(2f);
        dynamicView.addTransition(R.drawable.ic_word, transition.scaleTarget(1f));
        dynamicView.addTransition(R.drawable.ic_video, transition.scaleTarget(1f));
        dynamicView.addTransition(R.drawable.ic_graphic, transition.scaleTarget(1f));

        transition = (SimpleDynamicTransition) defFirstTransition().speedFactor(2f);
        dynamicView.addTransition(R.drawable.ic_music_36x48, transition);
        dynamicView.addTransition(R.drawable.ic_text_file_50x64, transition);

        transition = (SimpleDynamicTransition) defFirstTransition().speedFactor(1.6f);
        dynamicView.addTransition(R.drawable.ic_plane_40x26, transition);
        dynamicView.addTransition(R.drawable.ic_lock_14x18, transition);
        dynamicView.addTransition(R.drawable.ic_upload_40x28, transition);

        transition = (SimpleDynamicTransition) defFirstTransition().speedFactor(1.5f);
        dynamicView.addTransition(R.drawable.ic_10_gb_174x136, transition);

        transition = (SimpleDynamicTransition) defFirstTransition().speedFactor(1.3f);
        dynamicView.addTransition(R.drawable.ic_gb_96x65, transition);

        transition = (SimpleDynamicTransition) defFirstTransition().speedFactor(1.1f);
        dynamicView.addTransition(R.drawable.ic_free_90x30, transition);

        transition = (SimpleDynamicTransition) defFirstTransition().speedFactor(1f);
        dynamicView.addTransition(R.drawable.ic_music_22x30, transition);
        dynamicView.addTransition(R.drawable.ic_graphic_26x36, transition);
        dynamicView.addTransition(R.drawable.ic_text_file_24x34, transition);

        dynamicView.addTransition(new SimpleDynamicTransition(R.drawable.ic_box_158x120));
        dynamicView.addTransition(new SimpleDynamicTransition(R.drawable.ic_light).alphaStart(0));
        dynamicView.addTransition(new SimpleDynamicTransition(R.drawable.ic_favorite_36).scaleStart(0).xStart(100).yStart(200).rotateStart(0));
        dynamicView.addTransition(new SimpleDynamicTransition(R.drawable.ic_share_22x24).scaleStart(0).xStart(150).yStart(140).rotateStart(0));
        dynamicView.addTransition(new SimpleDynamicTransition(R.drawable.ic_offline_28).scaleStart(0).xStart(200).yStart(200).rotateStart(0));

        dynamicView.postDelayed(new Runnable() {
            @Override
            public void run() {
                dynamicView.jump(true, new Runnable() {
                    @Override
                    public void run() {
                        //TODO: set scrolling enable for Dynamic layout
                    }
                }, true);
            }
        }, 500);
    }

    SimpleDynamicTransition defFirstTransition() {
        return new SimpleDynamicTransition(0).xStart(162).yStart(286).rotateStart(0).alphaStart(0).scaleStart(0).startIsRelative(true);
    }

    SimpleDynamicTransition defSecondTransition() {
        return new SimpleDynamicTransition(0).xOutOfScreenTarget(SimpleDynamicTransition.OutOffScreenSide.Start).startIsRelative(true);
    }

    SimpleDynamicTransition defThirdTransition() {
        return new SimpleDynamicTransition(0).alphaStart(0).alphaTarget(0);
    }

    private final DynamicLayout.PrepareListener mPrepareListener = new DynamicLayout.PrepareListener() {

        @Override
        public int getStateCount() {
            return 3;
        }

        @Override
        public void prepareForState(DynamicView dynamicView, int state) {
            Log.e(TAG, "prepare for state: " + state);
            switch (state) {
                case 0: {
                    dynamicView.clearTransitions();
                    dynamicView.setSingleTransition(null);

                    dynamicView.removeObject(R.drawable.ic_shield_210x170);
                    dynamicView.removeObject(R.drawable.ic_lock_70x70);

                    dynamicView.removeObject(R.drawable.ic_user_1);
                    dynamicView.removeObject(R.drawable.ic_user_2);
                    dynamicView.removeObject(R.drawable.ic_user_3);
                    dynamicView.removeObject(R.drawable.ic_user_4);

                    dynamicView.removeObject(R.drawable.ic_plane_full_1);
                    dynamicView.removeObject(R.drawable.ic_plane_full_2);

                    break;
                }
                case 1: {

                    dynamicView.removeObject(R.drawable.ic_user_1);
                    dynamicView.removeObject(R.drawable.ic_user_2);
                    dynamicView.removeObject(R.drawable.ic_user_3);
                    dynamicView.removeObject(R.drawable.ic_user_4);

                    dynamicView.removeObject(R.drawable.ic_plane_full_1);
                    dynamicView.removeObject(R.drawable.ic_plane_full_2);

                    dynamicView.addObject(new DynamicDrawableObject(R.drawable.ic_shield_210x170).x(75).y(122));
                    dynamicView.addObject(new DynamicDrawableObject(R.drawable.ic_lock_70x70).x(186).y(270));

                    dynamicView.clearTransitions();
                    dynamicView.setSingleTransition(defSecondTransition());

                    dynamicView.addTransition(new SimpleDynamicTransition(R.drawable.ic_shield_210x170).yOutOfScreenStart(SimpleDynamicTransition.OutOffScreenSide.End).alphaStart(1f).alphaTarget(1f));
                    dynamicView.addTransition(new SimpleDynamicTransition(R.drawable.ic_lock_70x70).yOutOfScreenStart(SimpleDynamicTransition.OutOffScreenSide.End).alphaStart(1f).alphaTarget(1f));

                    SimpleDynamicTransition transition = (SimpleDynamicTransition) new SimpleDynamicTransition(1).scaleTarget(0f).rotateTarget(0).speedFactor(1.6f);
                    dynamicView.addTransition(R.drawable.ic_favorite_36, transition);
                    dynamicView.addTransition(R.drawable.ic_share_22x24, transition);
                    dynamicView.addTransition(R.drawable.ic_offline_28, transition);

                    dynamicView.addTransition(R.drawable.ic_word, new SimpleDynamicTransition(R.drawable.ic_word).xTarget(130).yTarget(64).scaleStart(1f).scaleTarget(1.7372f).rotateTarget(0));
                    dynamicView.addTransition(R.drawable.ic_video, new SimpleDynamicTransition(R.drawable.ic_video).xTarget(70).yTarget(84).scaleStart(1f).scaleTarget(1.5555f).rotateTarget(-30));
                    dynamicView.addTransition(R.drawable.ic_graphic, new SimpleDynamicTransition(R.drawable.ic_graphic).xTarget(200).yTarget(84).scaleStart(1f).scaleTarget(1.461f).rotateTarget(30));

                    break;
                }
                case 2: {

                    dynamicView.removeObject(mGraphicObject);
                    dynamicView.removeObject(mVideoObject);
                    dynamicView.removeObject(mWordObject);

                    dynamicView.removeObject(R.drawable.ic_shield_210x170);
                    dynamicView.removeObject(R.drawable.ic_lock_70x70);

                    dynamicView.addObject(new DynamicDrawableObject(R.drawable.ic_user_1).x(11).y(30));
                    dynamicView.addObject(new DynamicDrawableObject(R.drawable.ic_user_2).x(263).y(5));
                    dynamicView.addObject(new DynamicDrawableObject(R.drawable.ic_user_3).x(204).y(293));
                    dynamicView.addObject(new DynamicDrawableObject(R.drawable.ic_user_4).x(27).y(306));

                    dynamicView.addObject(new DynamicDrawableObject(R.drawable.ic_plane_full_1).x(3).y(66));
                    dynamicView.addObject(new DynamicDrawableObject(R.drawable.ic_plane_full_2).x(184).y(26));

                    dynamicView.addObject(mGraphicObject);
                    dynamicView.addObject(mVideoObject);
                    dynamicView.addObject(mWordObject);

                    dynamicView.addObject(new DynamicDrawableObject(R.drawable.ic_shield_210x170).x(75).y(122));
                    dynamicView.addObject(new DynamicDrawableObject(R.drawable.ic_lock_70x70).x(186).y(270));


                    dynamicView.clearTransitions();
                    dynamicView.setSingleTransition(defThirdTransition());

                    dynamicView.addTransition(new SimpleDynamicTransition(R.drawable.ic_user_1).alphaStart(0).scaleStart(0f));//.speedFactor(4f));
                    dynamicView.addTransition(new SimpleDynamicTransition(R.drawable.ic_user_2).alphaStart(0).scaleStart(0f));//.speedFactor(3f));
                    dynamicView.addTransition(new SimpleDynamicTransition(R.drawable.ic_user_3).alphaStart(0).scaleStart(0f));//.speedFactor(2f));
                    dynamicView.addTransition(new SimpleDynamicTransition(R.drawable.ic_user_4).alphaStart(0).scaleStart(0f));//.speedFactor(1f));

                    dynamicView.addTransition(new SimpleDynamicTransition(R.drawable.ic_plane_full_1).alphaStart(0));//.interpolator(new AccelerateInterpolator(3f)));
                    dynamicView.addTransition(new SimpleDynamicTransition(R.drawable.ic_plane_full_2).alphaStart(0));//.interpolator(new AccelerateInterpolator(3f)));

                    dynamicView.addTransition(R.drawable.ic_word, new SimpleDynamicTransition(R.drawable.ic_word).xStart(130).yStart(64).xTarget(130).yTarget(150)
                            .scaleTarget(1.8668f).scaleStart(1.7372f).rotateStart(0).rotateTarget(0));
                    dynamicView.addTransition(R.drawable.ic_video, new SimpleDynamicTransition(R.drawable.ic_video).xStart(70).yStart(84).xTarget(60).yTarget(174)
                            .scaleTarget(1.6666f).scaleStart(1.5555f).rotateStart(-30).rotateTarget(-20));
                    dynamicView.addTransition(R.drawable.ic_graphic, new SimpleDynamicTransition(R.drawable.ic_graphic).xStart(200).yStart(84).xTarget(210).yTarget(174)
                            .scaleTarget(1.5653f).scaleStart(1.461f).rotateStart(30).rotateTarget(26));


                    dynamicView.addTransition(new SimpleDynamicTransition(R.drawable.ic_shield_210x170).yOutOfScreenTarget(SimpleDynamicTransition.OutOffScreenSide.End).alphaStart(1f).alphaTarget(1f));
                    dynamicView.addTransition(new SimpleDynamicTransition(R.drawable.ic_lock_70x70).yOutOfScreenTarget(SimpleDynamicTransition.OutOffScreenSide.End).alphaStart(1f).alphaTarget(1f));

                    break;
                }
            }
        }

        @Override
        public CharSequence getTitle(int section) {
            switch (section) {
                case 0: {
                    return "Store, Access and Share\n" +
                            "All your Data Anywhere!";
                }
                case 1: {
                    return "Total Security";
                }
                case 2: {
                    return "Sharing and Collaboration";
                }
            }
            return "Section #" + section;
        }
    };

}
